var expect = chai.expect;

describe("Javascript", function() {
    describe("Global variables", function() {
        it('Unfortunately, they can exist', function() {
            JS.declareAGlobalVariable();
            expect(theGlobalVariable).to.equal('Gooood!');
        });
    });
    describe("Types", function() {
        describe("Numbers", function() {
            it('Are numbers', function() {
                expect(JS.giveMeANumber()).to.be.a('number');
                expect(JS.giveMeANumber()).not.to.be.a('string');
                expect(JS.giveMeANumber()).not.to.be.a('function');
                expect(JS.giveMeANumber()).not.to.be.a('boolean');
                expect(JS.giveMeANumber()).not.to.be.a('array');
                expect(JS.giveMeANumber()).not.to.be.a('object');
                expect(JS.giveMeANumber()).not.to.be.null;
                expect(JS.giveMeANumber()).not.to.be.undefined;
                expect(JS.giveMeANumber()).not.to.be.a('regexp');
            });
            it('They are all double precision', function() {
                expect(JS.giveMeANumber()).to.be.greaterThan(0);
                expect(JS.giveMeANumber()).to.be.lessThan(1);
            });
            it('Cannot have properties', function() {
                var aNumber = JS.giveMeANumber();
                aNumber.someProperty = 'someValue';
                expect(aNumber).not.to.have.property('someProperty');
            });
        });
        describe("Strings", function() {
            it('Are strings', function() {
                expect(JS.giveMeAString()).to.be.a('string');
                expect(JS.giveMeAString()).not.to.be.a('number');
                expect(JS.giveMeAString()).not.to.be.a('function');
                expect(JS.giveMeAString()).not.to.be.a('boolean');
                expect(JS.giveMeAString()).not.to.be.a('array');
                expect(JS.giveMeAString()).not.to.be.a('object');
                expect(JS.giveMeAString()).not.to.be.null;
                expect(JS.giveMeAString()).not.to.be.undefined;
                expect(JS.giveMeAString()).not.to.be.a('regexp');
            });
            it('Can be single quoted', function() {
                expect(JS.giveMeAString()).to.equal('WDS');
            });
            it('Can be double quoted', function() {
                expect(JS.giveMeAString()).to.equal("WDS");
            });
            it('Are case sensitive', function() {
                expect(JS.giveMeAString()).not.to.equal("WdS");
            });
            it('Cannot have properties', function() {
                var aString = JS.giveMeAString();
                aString.someProperty = 'someValue';
                expect(aString).not.to.have.property('someProperty');
            });
            it('But have length property', function() {
                expect(JS.giveMeAString()).have.length(3);
            });
            it('Give access to any character', function() {
                expect(JS.giveMeTheCharacterInAString(3, 'TheString')).to.equal('e');
            });
            it('Can be concatenated', function() {
                expect(JS.concatenateStrings('Star', 'Wars')).to.equal('Star Wars');
            });
        });
        describe("Functions", function() {
            it('Are functions', function() {
                expect(JS.giveMeAFunction()).to.be.a('function');
                expect(JS.giveMeAFunction()).not.to.be.a('number');
                expect(JS.giveMeAFunction()).not.to.be.a('string');
                expect(JS.giveMeAFunction()).not.to.be.a('boolean');
                expect(JS.giveMeAFunction()).not.to.be.a('array');
                expect(JS.giveMeAFunction()).not.to.be.a('object');
                expect(JS.giveMeAFunction()).not.to.be.null;
                expect(JS.giveMeAFunction()).not.to.be.undefined;
                expect(JS.giveMeAFunction()).not.to.be.a('regexp');
            });
            it('Can be called standalone and "this" will be the global object', function() {
                var whoIsThis, theArg1, theArg2;
                JS.callThisFunction(function(arg1, arg2) {
                    theArg1 = arg1;
                    theArg2 = arg2;
                    whoIsThis = this;
                });
                expect(whoIsThis).to.equal(window);
                expect(theArg1).to.equal('It');
                expect(theArg2).to.equal('Works');
            });
            it('Can be applied to objects and "this" will be the object', function() {
                var iAmThis = {}, whoIsThis, theArg1, theArg2;
                JS.applyThisFunctionToThisObject(function(arg1, arg2) {
                    theArg1 = arg1;
                    theArg2 = arg2;
                    whoIsThis = this;
                }, iAmThis);
                expect(whoIsThis).to.equal(iAmThis);
                expect(theArg1).to.equal('It');
                expect(theArg2).to.equal('Works');
            });
            it('Can be called with as many arguments as you want', function() {
                var theArg1, theArg2, theArg3;
                JS.callThisFunctionWithManyArguments(function(arg1, arg2) {
                    theArg1 = arg1;
                    theArg2 = arg2;
                    theArg3 = arguments[2];
                });
                expect(theArg1).to.equal('It');
                expect(theArg2).to.equal('Works');
                expect(theArg3).to.equal('Yeah!');
            });
            it('Call inner functions returned as a result for obtaining the right value', function() {
                function outerFunction(x) {
                    function innerFunction(y) {
                        return x + y;
                    }
                    return innerFunction;
                };
                expect(JS.callThisFunctionWithTheRightParameters(outerFunction)).to.equal(8);
            });
            it('Can have properties', function() {
                var aFunction = JS.giveMeAFunction();
                aFunction.someProperty = 'someValue';
                expect(aFunction).to.have.property('someProperty').and.equal('someValue');
            });
        });
        describe("Booleans", function() {
            it('Are booleans', function() {
                expect(JS.giveMeABoolean()).to.be.a('boolean');
                expect(JS.giveMeABoolean()).not.to.be.a('function');
                expect(JS.giveMeABoolean()).not.to.be.a('number');
                expect(JS.giveMeABoolean()).not.to.be.a('string');
                expect(JS.giveMeABoolean()).not.to.be.a('array');
                expect(JS.giveMeABoolean()).not.to.be.a('object');
                expect(JS.giveMeABoolean()).not.to.be.null;
                expect(JS.giveMeABoolean()).not.to.be.undefined;
                expect(JS.giveMeABoolean()).not.to.be.a('regexp');
            });
        });
        describe("Arrays", function() {
            it('Are arrays', function() {
                expect(JS.giveMeAnArray()).to.be.an('array');
                expect(JS.giveMeAnArray()).not.to.be.a('boolean');
                expect(JS.giveMeAnArray()).not.to.be.a('function');
                expect(JS.giveMeAnArray()).not.to.be.a('number');
                expect(JS.giveMeAnArray()).not.to.be.a('string');
                expect(JS.giveMeAnArray()).not.to.be.a('object');
                expect(JS.giveMeAnArray()).not.to.be.null;
                expect(JS.giveMeAnArray()).not.to.be.undefined;
                expect(JS.giveMeAnArray()).not.to.be.a('regexp');
            });
            it('Have a length property', function() {
                expect(JS.giveMeAnArray()).have.length(5);
            });
            it('Give access to any element', function() {
                expect(JS.giveMeTheElementInAnArray(3, [1, 2, 3, 4])).to.equal(4);
            });
            it('Can be concatenated', function() {
                var a = [1, 2], b = [3, 4];
                expect(JS.concatenateArrays(a, b)).to.deep.equal([1, 2, 3, 4]);
                expect(a).to.deep.equal([1, 2]);
                expect(b).to.deep.equal([3, 4]);
            });
            it('Can have elements added', function() {
                var a = [1, 2];
                JS.addElementToArray(a, 3);
                expect(a).to.deep.equal([1, 2, 3]);
            });
        });
        describe("Objects", function() {
            it('Are objects', function() {
                expect(JS.giveMeAnObject()).to.be.an('object');
                expect(JS.giveMeAnObject()).not.to.be.a('number');
                expect(JS.giveMeAnObject()).not.to.be.a('string');
                expect(JS.giveMeAnObject()).not.to.be.a('function');
                expect(JS.giveMeAnObject()).not.to.be.a('boolean');
                expect(JS.giveMeAnObject()).not.to.be.a('array');
                expect(JS.giveMeAnObject()).not.to.be.null;
                expect(JS.giveMeAnObject()).not.to.be.undefined;
                expect(JS.giveMeAnObject()).not.to.be.a('regexp');
            });

            it('Can have properties of any kind (but undefined)', function() {
                var anObject = JS.giveMeAnObject();
                JS.fillObjectWithProperties(anObject);
                expect(anObject).to.have.property('someNumber').and.be.a('number');
                expect(anObject).to.have.property('someString').and.be.a('String');
                expect(anObject).to.have.property('someFunction').and.be.a('function');
                expect(anObject).to.have.property('someBoolean').and.be.a('boolean');
                expect(anObject).to.have.property('someObject').and.be.a('object');
                expect(anObject).to.have.property('someArray').and.be.a('array');
                expect(anObject).to.have.property('aNull').and.be.null;
            });

            it('Can have properties removed', function() {
                var anObject = JS.giveMeAnObject();
                JS.fillObjectWithProperties(anObject);
                JS.deleteTheRightPropertyFromThisObject(anObject);
                expect(anObject).not.to.have.property('someNumber');
            });
        });
        describe("Null", function() {
            it('Is null', function() {
                expect(JS.giveMeANull()).to.be.null;
                expect(JS.giveMeANull()).not.to.be.an('object');
                expect(JS.giveMeANull()).not.to.be.a('number');
                expect(JS.giveMeANull()).not.to.be.a('string');
                expect(JS.giveMeANull()).not.to.be.a('function');
                expect(JS.giveMeANull()).not.to.be.a('boolean');
                expect(JS.giveMeANull()).not.to.be.a('array');
                expect(JS.giveMeANull()).not.to.be.undefined;
                expect(JS.giveMeANull()).not.to.be.a('regexp');
            });
        });
        describe("Undefined", function() {
            it('Is undefined', function() {
                expect(JS.giveMeAnUndefined()).to.be.undefined;
                expect(JS.giveMeAnUndefined()).not.to.be.an('object');
                expect(JS.giveMeAnUndefined()).not.to.be.a('number');
                expect(JS.giveMeAnUndefined()).not.to.be.a('string');
                expect(JS.giveMeAnUndefined()).not.to.be.a('function');
                expect(JS.giveMeAnUndefined()).not.to.be.a('boolean');
                expect(JS.giveMeAnUndefined()).not.to.be.a('array');
                expect(JS.giveMeAnUndefined()).not.to.be.null;
                expect(JS.giveMeAnUndefined()).not.to.be.a('regexp');
            });
        });
        describe("Regular Expressions", function() {
            it('Are first class citizens', function() {
                expect(JS.giveMeARegex()).to.be.a('regexp');
                expect(JS.giveMeARegex()).not.to.be.an('object');
                expect(JS.giveMeARegex()).not.to.be.a('number');
                expect(JS.giveMeARegex()).not.to.be.a('string');
                expect(JS.giveMeARegex()).not.to.be.a('function');
                expect(JS.giveMeARegex()).not.to.be.a('boolean');
                expect(JS.giveMeARegex()).not.to.be.a('array');
                expect(JS.giveMeARegex()).not.to.be.null;
                expect(JS.giveMeARegex()).not.to.be.undefined;
            });
            it('Can be tested on strings', function() {
                expect(JS.testRegex(/([A-Z][a-z]+){3}/, 'WirelessDataServices')).to.be.true;
            });
            it('Can be used to extract stuff', function() {
                var result = JS.getStuffFromString('WirelessDataServices');
                expect(result[0]).to.equal('Wireless');
                expect(result[1]).to.equal('Data');
                expect(result[2]).to.equal('Services');
            });
        });
    });
    describe("Truth", function() {
        describe("Truthy values", function() {
            it('A string is usually truthy', function() {
                expect(JS.giveMeAString()).to.be.a('string');
                expect(JS.giveMeAString()).to.be.ok;
            });
        });
        describe("Falsy values", function() {
            it('A string can be falsy', function() {
                expect(JS.giveMeAFalsyString()).to.be.a('string');
                expect(JS.giveMeAFalsyString()).not.to.be.ok;
            });
            it('A number can be falsy', function() {
                expect(JS.giveMeAFalsyNumber()).to.be.a('number');
                expect(JS.giveMeAFalsyNumber()).not.to.be.ok;
            });
            it('Undefined, null, and false are falsy', function() {
                expect(undefined).not.to.be.ok;
                expect(null).not.to.be.ok;
                expect(false).not.to.be.ok;
            });
        });
        describe("Operators", function() {
            describe("==", function() {
                it('Compares numbers', function() {
                    var values = JS.giveMeTwoEqualEqualNumbers();
                    expect(values[0]).to.be.a('number');
                    expect(values[1]).to.be.a('number');
                    expect(values[0] == values[1]).to.be.true;
                    var number = JS.giveMeANumberThatIsNotEqualToItself();
                    expect(number).to.be.a('number');
                    expect(number == number).to.be.false;
                });
                it('Compares strings', function(){
                    var values = JS.giveMeTwoEqualEqualStrings();
                    expect(values[0]).to.be.a('string');
                    expect(values[1]).to.be.a('string');
                    expect(values[0] == values[1]).to.be.true;
                });
                it('Compares arrays', function(){
                    var values = JS.giveMeTwoEqualEqualArrays();
                    expect(values[0]).to.be.an('array');
                    expect(values[1]).to.be.an('array');
                    expect(values[0] == values[1]).to.be.true;
                });
                it('Compares functions', function() {
                    var values = JS.giveMeTwoEqualEqualFunctions();
                    expect(values[0]).to.be.a('function');
                    expect(values[1]).to.be.a('function');
                    expect(values[0] == values[1]).to.be.true;
                });
                it('Compares objects', function() {
                    var values = JS.giveMeTwoEqualEqualObjects();
                    expect(values[0]).to.be.a('object');
                    expect(values[1]).to.be.a('object');
                    expect(values[0] == values[1]).to.be.true;
                });
                 it('Compares wierdly', function() {
                    var value = JS.giveMeAValueEqualEqualToAllOfThis();
                    expect(value == 0).to.be.true;
                    expect(value == '0').to.be.true;
                    expect(value == '').to.be.true;
                    expect(value == '  \n\t\r').to.be.true;
                    expect(value == undefined).to.be.false;
                    expect(value == null).to.be.false;
                });
            });
            describe("!=", function() {
                it('Works the opposite as ==');
            });
            describe("===", function() {
                it('Compares things for strict inequality', function(){
                    var value = JS.giveMeAStringEqualEqualToABC();
                    expect(value == 'abc').to.be.true;
                    expect(value === 'abc').to.be.false;
                });
                it('Things have to be == and of the same type', function(){
                    expect( undefined === null).to.be.false;
                    expect( ' ' === 0).to.be.false;
                    expect( null === 0).to.be.false;
                    expect( undefined === 0).to.be.false;
                });
            });
            describe("!==", function() {
                it('Works the opposite as ===');
            });
            describe("!", function() {
                it('Compares things for strict equality');
            });
            describe("||", function() {
                it('Use of OR evaluator when doin assignations',function(){
                    var value = JS.giveMeAValueWhenEvaluatedByOROpIsNotAssigned();
                    expect( value || 5 ).to.equal(5);
                });
            });
            describe("&&", function() {
                it('Compares things for strict inequality');
            });
            describe(">", function() {
                it('Compares things for strict inequality');
            });
            describe("<", function() {
                it('Compares things for strict inequality');
            });
            describe(">=", function() {
                it('Compares things for strict inequality');
            });
            describe("<=", function() {
                it('Compares things for strict inequality');
            });
        });
        describe("Conditionals", function() {
            describe("If", function() {
                it('Exists', function() {
                    expect(JS.simpleIfStatement(false)).to.be.true;
                });
                it('Supports the else clause', function() {
                    expect(JS.simpleIfStatement(true)).to.be.false;
                });
            });
            describe("Switch", function() {
                it('Is similar to the java one', function() {
                    expect(JS.simpleSwitch(0)).to.equal('1');
                    expect(JS.simpleSwitch(1)).to.equal('2');
                    expect(JS.simpleSwitch(2)).to.equal('3');
                    expect(JS.simpleSwitch(3)).to.equal('4');
                    expect(JS.simpleSwitch(4)).to.equal('5');
                });
                it('Supports strings as cases', function() {
                    expect(JS.simpleSwitch('0')).to.equal(0);
                    expect(JS.simpleSwitch('1')).to.equal(1);
                    expect(JS.simpleSwitch('2')).to.equal(2);
                    expect(JS.simpleSwitch('3')).to.equal(3);
                    expect(JS.simpleSwitch(-1)).to.equal('default');
                    expect(JS.simpleSwitch(-2)).to.equal('default');
                    expect(JS.simpleSwitch(-3)).to.equal('default');
                    expect(JS.simpleSwitch(-4)).to.equal('default');
                });
                it('Supports the default case', function() {
                    expect(JS.simpleSwitch(-1)).to.equal('default');
                    expect(JS.simpleSwitch(-2)).to.equal('default');
                    expect(JS.simpleSwitch(-3)).to.equal('default');
                    expect(JS.simpleSwitch(-4)).to.equal('default');
                });
            });
            describe("Inline conditional", function() {
                it('Is similar to the java one');
            });
        });
    });
    describe("Loops", function() {
        describe("For", function() {
            it('Should something');
        });
        describe("While", function() {
            it('Should something');
        });
        describe("For each", function() {
            it('Should something');
        });
    });
    describe("OOP", function() {
        describe("Objects", function() {
            it('Should something');
        });
        describe("Classical", function() {
            it('Can create classes from functions', function() {
                var Class = JS.giveMeAClass();

                expect(Class.prototype).not.to.have.property('data');
                expect(Class.prototype).not.to.have.property('doStuff');

                var instance = JS.giveMeAnInstance(Class);

                expect(instance.__proto__).to.equal(Class.prototype);
                expect(instance).to.have.property('data').and.be.a('string');
                expect(instance).to.have.property('doStuff').and.be.a('function');
            });
            it('The instances do not share properties, that consumes memory', function() {
                var Class = JS.giveMeAClass();

                var instance1 = new Class();
                var instance2 = new Class();

                expect(instance1.doStuff).not.to.equal(instance2.doStuff);
            });
        });
        describe("Prototypal", function() {
            it('Can create classes from prototypes', function() {
                var Class = JS.giveMeAPrototypeClass();

                expect(Class.prototype).to.have.property('data');
                expect(Class.prototype).to.have.property('doStuff');

                var instance = JS.giveMeAnInstance(Class);

                expect(instance.__proto__).to.equal(Class.prototype);
                expect(instance).to.have.property('data').and.be.a('string');
                expect(instance).to.have.property('doStuff').and.be.a('function');
            });
            it('All the instances share the same properties from the prototype, that is good', function() {
                var Class = JS.giveMeAPrototypeClass();

                var instance1 = new Class();
                var instance2 = new Class();

                expect(instance1.doStuff).to.equal(instance2.doStuff);
            });
        });
    });
    describe("Exceptions", function() {
        it('Can be thrown', function() {
            expect(JS.throwSomething).to.throw('Something');
        });
        it('Can be caught', function() {
            var exception = JS.catchIt(function() {
                JS.throwSomething()
            });
            expect(exception).to.equal('Something');
        });
    });
});