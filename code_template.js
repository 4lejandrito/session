var JS = {
    declareAGlobalVariable: function(){
    },
    giveMeANumber: function() {
    },
    giveMeANumberThatIsNotEqualToItself: function() {
    },
    giveMeAFalsyNumber: function() {
    },
    giveMeAString: function() {
    },
    giveMeTheCharacterInAString: function(theIndex, theString) {
    },
    concatenateStrings: function(a, b) {
    },
    giveMeAFalsyString: function() {
    },
    giveMeAFunction: function() {
    },
    callThisFunction: function(theFunction) {
    },
    applyThisFunctionToThisObject: function(theFunction, theObject) {
    },
    callThisFunctionWithManyArguments: function(theFunction) {
    },
    callThisFunctionWithTheRightParameters: function(theFunction){
    },
    giveMeTwoEqualEqualNumbers: function(){
    },
    giveMeTwoEqualEqualStrings: function() {
    },
    giveMeTwoEqualEqualArrays: function(){
    },
    giveMeTwoEqualEqualFunctions: function(){
    },
    giveMeTwoEqualEqualObjects: function(){
    },
    giveMeAValueEqualEqualToAllOfThis: function(){
    },
    giveMeAStringEqualEqualToABC: function(){
    },
    giveMeAValueWhenEvaluatedByOROpIsNotAssigned: function(){
    },
    giveMeABoolean: function() {
    },
    giveMeAnArray: function() {
    },
    giveMeTheElementInAnArray: function(theIndex, theArray) {
    },
    concatenateArrays: function(a, b) {
    },
    addElementToArray: function(theArray, theElement) {
    },
    giveMeAnObject: function() {
    },
    fillObjectWithProperties: function(theObject) {
    },
    deleteTheRightPropertyFromThisObject: function(theObject) {
    },
    giveMeANull: function() {
    },
    giveMeAnUndefined: function() {
    },
    giveMeARegex: function() {
    },
    testRegex: function(regexp, theString) {
    },
    getStuffFromString: function(theString) {
    },
    simpleIfStatement: function(value) {
    },
    simpleSwitch: function(value) {
    },
    giveMeAClass: function(){
    },
    giveMeAnInstance: function(Class){
    },
    giveMeAPrototypeClass: function(){
    },
    throwSomething: function(){
    },
    catchIt: function(theFunction){
    }
}